# edna_metabarcoding_18S_sand_extraction

## eDNA metabarcoding of benthic nematodes and Machine Learning for Sand Extraction impact assessment

## Project Abstract

Effects of sand extraction, a socio-economically important industry in the Belgian Part of the North Sea, are monitored through macrobenthos-based indices, an expensive and time-consuming process. While eDNA metabarcoding and supervised machine learning can circumvent these issues, area and disturbance-specific protocols are needed. This study aimed to firstly explore if 18S eDNA metabarcoding data detects sand extraction effects on nematode diversity, a promising but underutilized indicator taxa;  secondly, if this data can predict ecosystem status with SML. Sediment samples (n=86) were used for environmental DNA sequencing and morphologically identification of macrobenthos, after which alpha and beta diversity were analyzed. LASSO regression was used to predict BEQI ecosystem assessment. Analyses were performed at ASV and genus level to explore if taxon specificity affects predictive accuracy. 
Extraction intensity had no effect on ASV alpha diversity but genus level data showed significant differences. Beta diversity was significantly different between high and low/no extraction in Thorntonbank. No significant difference was found for Oostdyck or Hinderbanken. Predicted ecological status did not have enough agreement with macrobenthos-based assessment, either at ASV or genus level. We highlight future steps to increase predictive accuracy of a machine learning approach for impact assessments of sand extraction activity. 


## Files Description
The R scripts used to achieve this project are as follows:

01_Dada2_18S: Modification of Dada2 bioinformatic pipeline to filter, clean, merge, remove chimeras and rarefy reads from 18S high throughput sequencing data from sediment extracted DNA. Taxonomy is assigned using assignTaxonomy function, prioritizing small and specific reference databases, and moving on to a general one if no taxonomy was assigned. Contaminant ASVs are identified and removed. Final output is the count table of reads per ASVs. 

02_Diversity_Analyses_Nematodes_eDNA: using the imported count table, this file explores sand extraction effects on alpha diveristy of nematodes at genus and ASV level through richness, Shannon-Wiener index and Inverse Simpson. Differences are explored with ANOVA and emmeans post hoc tests. Effects on beta diveristy are explored through bray-curtis dissimilarity and NMDS with PERMANOVA and pairwise comparison using adonis. 

03_Bray_Curtis_Calculation_Morphology: This file uses the count table of morphologically identified macrobenthos to calculate bray curtis dissimilarity between samples, which is needed for the LASSO regression in the next file.

04_18S_LASSO_Regression_ETC: this file uses 18S eDNA metabarcoding data, morphological identification data as well as sediment granulometry and sediment extraction intensity and frequency to fit a LASSO regression. Next, predictions are made using 18S eDNA metabarcoding data for four macrobenthos parameters, namely species richness, density, biomass and community similarity. The predicted values per station are used for BEQI calculations in the last script. 

05_BEQI_Reference_and_Kappa: This file calculates BEQI reference values and assigns an ecological health status score from predicted values. Next, it compares status from predictions and from macrobenthos observed values, and determines agreement between the two using Cohen's kappa coefficient. 

These are all R markdown files run in R. 



## License
If you have any questions, do not hesitate to reach out. 

